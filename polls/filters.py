import django_filters
from .models import *

class SamplesFilter(django_filters.FilterSet):
    class Meta:
        model = Samples
        fields = {'SampleID':['icontains'], 'Organ':['icontains'], 'TumorType':['icontains']}


class CulturesFilter(django_filters.FilterSet):
    class Meta:
        model = CultureInfo
        fields = {'SubtypeID':['icontains'], 'SubtypeInfo':['icontains'], 'Passage':['icontains']}

class MolecularOrderFilter(django_filters.FilterSet):
    class Meta:
        model = MolecularOrder
        fields = {'MolecularOrderID':['icontains']}
class FreezingFilter(django_filters.FilterSet):
    class Meta:
        model = Freezing
        fields = {'FreezingID':['icontains'], 'Type':['icontains']}
 
