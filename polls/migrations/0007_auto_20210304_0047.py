# Generated by Django 2.1.15 on 2021-03-04 00:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0006_auto_20210304_0034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='processing',
            name='DateBlock',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='processing',
            name='DefrozePassage',
            field=models.IntegerField(null=True),
        ),
    ]
