import django_tables2
from django_tables2.utils import A  # alias for Accessor
from .models import *
from django import forms

class SamplesTable(django_tables2.Table):
    Cultures = django_tables2.LinkColumn('culturesView', text='view', args=[A('pk')])
    Edit = django_tables2.LinkColumn('editSamples', text='edit', args=[A('pk')])
    Freezing = django_tables2.LinkColumn('freezing', text='Information', args=[A('pk')])
    class Meta:
        model = Samples

class CulturesTable(django_tables2.Table):

    Order = django_tables2.LinkColumn('molecularorder', text='view', args=[A('pk')])
    PassagesInfo = django_tables2.LinkColumn('passages', text='passages', args=[A('pk')])
    Edit = django_tables2.LinkColumn('editCulture', text='edit', args=[A('pk')])
    Delete = django_tables2.LinkColumn('deleteCulture', text='edit', args=[A('pk')])
    class Meta:
        model = CultureInfo
        exclude = ("CellBlock", "PatientConsent", "DateOfConsent", "Comments",)


class MolecularOrderTable(django_tables2.Table):
    Edit = django_tables2.LinkColumn('editMolecularOrder', text='edit', args=[A('pk')])
    class Meta:
        model = MolecularOrder

class ProcessingTable(django_tables2.Table):
    Edit = django_tables2.LinkColumn('editPassages', text='edit', args=[A('pk')])
    
    class Meta:
        model = Processing
        unlocalize = ("PassageDate", "DateBlock",)
        

class FreezingTable(django_tables2.Table):
    Edit = django_tables2.LinkColumn('editFreezing', text='edit', args=[A('pk')])
    class Meta:
        model = Freezing
