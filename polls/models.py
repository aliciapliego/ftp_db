#root /path/to/models.py
#models package from Django.db

import django 
from django.db import models 

class Samples(models.Model):
    SampleID = models.CharField(primary_key = True, max_length = 20)
    SampleName = models.CharField(max_length = 50, null = True, blank = True)
    Organ = models.CharField(max_length = 40, null = True)
    TumorType = models.CharField(max_length = 50, null = True)
    Additional = models.CharField(max_length = 40, null= True)
    MolecularGeneral = models.CharField(max_length = 40, null=True)
    MolecularOrganoid = models.CharField(max_length = 40, null = True)
    GeneralConsent = models.CharField(max_length = 40, null = True)
    def __str__(self):
        return self.SampleID

class Tissue(models.Model):
    TissueID = models.CharField('TissueID', primary_key = True, max_length = 20)
    SampleID = models.ForeignKey(Samples, max_length = 20, on_delete = models.CASCADE)
    def __str__(self):
        return self.TissueID

class PatientInfo(models.Model):
    PatientID = models.CharField(primary_key = True, max_length = 20)
    TissueID  = models.ForeignKey(Tissue, max_length = 20, on_delete = models.CASCADE)
    Name      = models.CharField(max_length = 30, null=True)
    Surname   = models.CharField(max_length = 40, null= True)
    Gender    = models.CharField(max_length = 20, null=True)

class Biobank(models.Model):
    BiobankID = models.CharField(primary_key = True, max_length = 20)
    TissueID  = models.ForeignKey(Tissue, max_length = 20, on_delete = models.CASCADE)
    def __str__(self):
        return self.BiobankID
        
class CultureInfo(models.Model):
    SubtypeID        = models.CharField(primary_key = True, max_length = 20)
    SampleID         = models.ForeignKey(Samples, max_length = 20, on_delete=models.CASCADE)
    SubtypeInfo      = models.CharField(max_length = 30, null = True)
    FirstCulture     = models.DateTimeField('First Passage',blank = True, null = True)
    CultureStatus    = models.CharField(max_length = 40, null = True, blank = True)
    Passage          = models.CharField(max_length = 30, null = True, blank = True)
    DefrozePassage   = models.CharField(max_length = 40, null = True, blank = True)
    Media            = models.CharField(max_length = 30, null = True, blank=True)
    Type             = models.CharField(max_length = 40, null = True, blank = True)
    CultureType      = models.CharField(max_length = 30, null=True, blank=True)
    Status           = models.CharField(max_length = 40, null = True, blank = True)
    LastCulture      = models.DateTimeField('Last Passage',blank = True, null = True)
    CellBlock        = models.CharField(max_length = 30, null= True, blank=True)
    KnownMutation    = models.CharField(max_length = 30, null= True, blank=True)
    MolecularOrder   = models.CharField(max_length = 10, null= True, blank=True)
    PatientConsent   = models.CharField(max_length = 30, null = True, blank=True)
    DateOfConsent    = models.DateTimeField('Date of Consent', null = True, blank=True)
    Note             = models.CharField(max_length = 120, null = True, blank = True)
    Note2            = models.CharField(max_length = 120, null = True, blank = True)
    def __str__(self):
        return self.SubtypeID

class Mutations(models.Model):
    MutationID         = models.CharField(primary_key = True, max_length = 20)
    SubtypeID          = models.ForeignKey(CultureInfo, max_length = 20, on_delete = models.CASCADE)
    GeneName           = models.CharField(max_length = 30, null = True)
    Chromosome         = models.CharField(max_length = 10, null = True)
    ChromosomePosition = models.IntegerField(null = True)
    GenePosition       = models.IntegerField(null = True)
    ProteinPosition    = models.IntegerField(null = True)

class MolecularOrder(models.Model):
    MolecularOrderID     = models.CharField(primary_key = True, max_length = 20)
    SubtypeID            = models.ForeignKey(CultureInfo, max_length = 20, on_delete = models.CASCADE)
    MolecularOrderNumber = models.CharField(max_length = 30, null = True)
    AssayType            = models.CharField(max_length = 30, null = True)

class Processing(models.Model):
    ProcessingID  = models.CharField(primary_key = True, max_length = 20)
    SubtypeID     = models.ForeignKey(CultureInfo, max_length = 20, on_delete = models.CASCADE)
    TumorType     = models.CharField(max_length = 50, null = True, blank = True)
    PassageDate   = models.DateTimeField(null = True)
    Media         = models.CharField(max_length = 40, null = True, blank = True)
    PassageNumber = models.IntegerField(null = True, blank = True)
    DefrozePassage= models.IntegerField(null = True, blank = True)
    Status        =  models.CharField(max_length = 50, null = True, blank = True)
    Format        = models.CharField(max_length = 50, null = True, blank = True)
    CultureType   = models.CharField(max_length = 40, null = True)
    FormatNumber  =models.CharField(max_length = 40, null = True)
    CellBlock     = models.CharField(max_length = 40, null = True)
    Smear         = models.CharField(max_length = 40, null = True)
    DateBlock     = models.DateTimeField( null = True, blank = True)
    Comments      = models.TextField(blank = True, null = True)



class Freezing(models.Model):
    FreezingID   = models.CharField(primary_key = True, max_length = 25)
    Type         = models.CharField(max_length = 40, null = True)
    SampleID     = models.ForeignKey(Samples, max_length = 25, on_delete = models.CASCADE)
    DefrozePassage= models.IntegerField(null = True, blank = True)
    Date         =  models.DateTimeField( null = True)
    PassageNumber = models.IntegerField(null = True, blank = True)
    Amount       = models.IntegerField(null = True, blank = True)
    FreezingType = models.CharField(max_length = 40, null = True, blank = True)
    Location     = models.CharField(max_length = 40, null = True)
    Tower        = models.CharField(max_length = 30, null = True)
    Box          = models.CharField(max_length = 40, null = True)
    Start        =  models.CharField(max_length = 40, null = True)
    End          =  models.CharField(max_length = 40, null = True)
    FrozenBy     =  models.CharField(max_length = 40, null = True)
    VialsRemoved =  models.CharField(max_length = 40, null = True)
    DateRemoved  = models.DateTimeField( null = True)
    BoxRemoved   = models.CharField(max_length = 40, null = True)
    StartRemoved =  models.CharField(max_length = 40, null = True)
    EndRemoved   =  models.CharField(max_length = 40, null = True)
    RemovedBy    =  models.CharField(max_length = 40, null = True)
    SentTo       =  models.CharField(max_length = 40, null = True)



