from django import forms

from polls.models import Samples
from polls.models import CultureInfo
from polls.models import MolecularOrder
from polls.models import Processing
from polls.models import Freezing
import datetime

class CultureInfoForm(forms.ModelForm):
    
    class Meta:
        model = CultureInfo
        exclude = []
        # = ['passage', 'subtype']

class SamplesForm(forms.ModelForm):

    class Meta:
        model = Samples
        exclude = []
        # = ['passage', 'subtype']

class MolecularOrderForm(forms.ModelForm):

    class Meta:
        model = MolecularOrder
        exclude = []
        # = ['passage', 'subtype']

class PassagesForm(forms.ModelForm):

    class Meta:
        model = Processing
        exclude = []
        # = ['passage', 'subtype']

class FreezingForm(forms.ModelForm):

    class Meta:
        model = Freezing
        exclude = []
        # = ['passage', 'subtype']
