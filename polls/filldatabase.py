from polls.models import Samples
import string

Samples.objects.create(
    SampleID = '123450',
    SampleName = 'SKM4',
    CancerType = 'Sarcoma',
)

#Retrieving records
samples_list = Samples.objects.all()

#For deleting 
#Samples.objects.filter(SampleName = 'SKM3').delete()


#Import data to Cultures
from polls.models import CultureInfo


culture = CultureInfo(
    SubtypeID = '234567',
    SampleID == '123450',
    SubtypeInfo = 'Sarcomadefault',
    FirstCulture = '2020-12-10',
    Passage = '2',
    Media = 'DMSO',
    CultureType = 'Par',
    CellBlock = 'Example',
    KnownMutation = '',
    MolecularOrder = '',
    PatientConsent = '',
    DateOfConsent = '2020-12-10',
    Comments = '',


)

from polls.models import CultureInfo, Samples, Processing

a = Samples(
    SampleID = '123459',
    SampleName = 'SKM9',
    CancerType = 'Sarcoma',
)

a.save()

b = CultureInfo(
    SampleID = a,
    SubtypeID = '234569',
    SubtypeInfo = 'Sarcomadefault',
    FirstCulture = '2020-12-15',
    Passage = '2',
    Media = 'DMSO',
    CultureType = 'Par',
    CellBlock = 'Example',
    KnownMutation = 'YES',
    MolecularOrder = '',
    PatientConsent = '',
    DateOfConsent = '2020-12-16',
    Comments = '',


)
b.save()

c = Processing(
    ProcessingID = '123459',
    SubtypeID = b,
    Subcategory = 'Sarcomadefault',
    PassageDate= '2020-12-10',
    Media = 'DMSO',
    PassageNumber = '3',
    CultureType = 'Normal',
    Comments = '',
)
c.save()


from polls.models import MolecularOrder

d = MolecularOrder(
    MolecularOrderID     = '1234567',
    SubtypeID            = b,
    MolecularOrderNumber = '0000',
    AssayType =''
)

d.save()


from polls.models import Freezing


e = Freezing(
    FreezingID   = '12345',
    ProcessingID = c,
    Amount       = 2,
    FreezingType = 'None',
    Location     = '23.Lab1',
    Tower        = '1',
    Box          = '234',
    Start        = '2020-12-16',
    End          = '2020-12-16',
    Comments     = '',
    FrozenBy     = 'Person1',

)
e.save()